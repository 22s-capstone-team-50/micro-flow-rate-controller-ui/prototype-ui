from re import M
import tkinter as tk
from tkinter import BOTH, CENTER, N, PAGES, Button, ttk
from turtle import right
import customtkinter
from pandas.tseries.offsets import Micro
from ttkthemes import ThemedStyle
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import numpy as np
from functools import partial
import matplotlib.animation as animation
from matplotlib import style
style.use('ggplot')
import math

customtkinter.set_default_color_theme("blue")

LARGEFONT=("Verdana", 35)
MEDFONT=("Verdana", 15)

class demoMicroFlowGraphApp(customtkinter.CTk):
    frames={"graphFlow": None, "help": None, "Settings": None}
    current = None
    mode="dark" #default mode is dark

    #Page Navigation:
    def PageNavigation (self, page):
        if demoMicroFlowGraphApp.frames[page] is not None:
            if demoMicroFlowGraphApp.current is not None:
                demoMicroFlowGraphApp.current.pack_forget()
                demoMicroFlowGraphApp.current=demoMicroFlowGraphApp.frames[page]
                demoMicroFlowGraphApp.frames[page].pack(in_=self.rightContainter, side=tk.TOP, fill=BOTH, expand=True)
            else:
                demoMicroFlowGraphApp.current=demoMicroFlowGraphApp.frames[page]
                demoMicroFlowGraphApp.frames[page].pack(in_=self.rightContainter, side=tk.TOP, fill=BOTH, expand=True)
    
    def toggleDark(self):
        customtkinter.set_appearance_mode("dark" if self.mode == "light" else "light")
        self.mode="dark" if self.mode == "light" else "light"
        self.darkModeSettings.configure(text="Dark Mode: OFF" if (self.mode=="light") else "Dark Mode: ON")

    def __init__(self):
        super().__init__()
        self.title("Micro Flow Rate App")
        self.geometry("800x600")

        main_containter=customtkinter.CTkFrame(self)
        main_containter.pack(fill=tk.BOTH, expand=True)

        #Left Side Panel for Frame Navigation
        leftPanel=customtkinter.CTkFrame(main_containter,width=150)
        leftPanel.pack(side=tk.LEFT,fill='y',expand=False)

        #Buttons for Navigation
        graphButton=customtkinter.CTkButton(leftPanel, text="Display Flow Rate",command=partial(self.PageNavigation,"graphFlow"))
        graphButton.grid(row=0,column=0)

        helpButton=customtkinter.CTkButton(leftPanel, text="Help", command=partial(self.PageNavigation, "help"))
        helpButton.grid(row=1, column=0)
        
        graphButton=customtkinter.CTkButton(leftPanel, text="Settings", command=partial(self.PageNavigation, "Settings"))
        graphButton.grid(row=2,column=0)


        #Right Panel for Display
        self.rightPanel=customtkinter.CTkFrame(main_containter)
        self.rightPanel.pack(side=tk.LEFT, fill=BOTH, expand=True)

        self.rightContainter=customtkinter.CTkFrame(self.rightPanel)
        self.rightContainter.pack(side=tk.LEFT, fill=BOTH, expand=True)

        #Graph Window:
        demoMicroFlowGraphApp.frames["graphFlow"]=customtkinter.CTkFrame(master=self, fg_color="#808080") ##3944BC"
        graphLabel=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["graphFlow"], text="Display Flow Rate", font=MEDFONT)
        graphLabel.place(relx=0.5, rely=0, anchor="n")

        sensirionLabel=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["graphFlow"], text="Sensirion Value: ", font=MEDFONT)
        sensirionLabel.place(relx=0.2, rely=0.2, anchor='n')

        referenceLabel=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["graphFlow"], text="Reference Flow: ", font=MEDFONT)
        referenceLabel.place(relx=0.8, rely=0.2, anchor="n")

        errorLabel=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["graphFlow"], text="Error: ", font=MEDFONT)
        errorLabel.place(relx=0.5, rely=0.6, anchor="n")
        
        errorValue=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["graphFlow"], text="12.5%", font=MEDFONT)
        errorValue.place(relx=0.5, rely=0.7, anchor="n")

        sensirionValue=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["graphFlow"], text="45 uL/min", font=MEDFONT)
        sensirionValue.place(relx=0.2, rely=0.3, anchor="n")

        referenceValue=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["graphFlow"], text="40uL/min", font=MEDFONT)
        referenceValue.place(relx=0.8, rely=0.3, anchor="n")


        #Help Window:
        demoMicroFlowGraphApp.frames["help"]=customtkinter.CTkFrame(master=self, fg_color="#808080") ##3944BC")
        helpLabel=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["help"], text="Help Manual", font=LARGEFONT)
        helpLabel.place(relx=0.5, rely=0, anchor=N)
        helpText=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["help"], text="""For real-time data plotting, press "Graph Flow" or use the hotkey shortcut "Ctrl-G".
For help, press the "Help" button or enter "Ctrl-H.
To forcibly quit the application, event "Ctrl-Q".
To access settings, press Control-R.

There are 3 Modes of Graphing: Flow Rate, Raw Data and Compare Error.
Flow Rate: Graphs Flow Rate (in uL/min) as a function of time
Raw Data: Graphs the Raw Sensirion Data (Volts) as a function of time
Compare Error: Compares the Error between the Raw Data and the Linearized Data, both as functions of time""", wraplength=500, font=MEDFONT)
        helpText.place(relx=0.5, rely=0.2, anchor=N)

        #Settings Window:
        demoMicroFlowGraphApp.frames["Settings"]=customtkinter.CTkFrame(master=self, fg_color="#808080")   ##3944BC")
        settingsLabel=customtkinter.CTkLabel(master=demoMicroFlowGraphApp.frames["Settings"], text="Settings", font=LARGEFONT)
        settingsLabel.place(relx=0.5, rely=0, anchor=N)
        self.darkModeSettings=customtkinter.CTkButton(master=demoMicroFlowGraphApp.frames["Settings"], text=("Dark Mode: ON (Default)"), command=self.toggleDark)
        self.darkModeSettings.place(relx=0.6, rely=0.2, anchor="e")
        
        #Hotkeys:
        def jumpHelp(event):
            if (demoMicroFlowGraphApp.current is not None): demoMicroFlowGraphApp.current.pack_forget()
            demoMicroFlowGraphApp.current=demoMicroFlowGraphApp.frames["help"]
            demoMicroFlowGraphApp.frames["help"].pack(in_=self.rightContainter, side=tk.TOP, fill=BOTH, expand=True)

        def jumpGraph(event):
            if (demoMicroFlowGraphApp.current is not None): demoMicroFlowGraphApp.current.pack_forget()
            demoMicroFlowGraphApp.current=demoMicroFlowGraphApp.frames["graphFlow"]
            demoMicroFlowGraphApp.frames["graphFlow"].pack(in_=self.rightContainter, side=tk.TOP, fill=BOTH, expand=True)

        def forceQuit(event):
            self.destroy()

        def jumpSettings(event):
            if (demoMicroFlowGraphApp.current is not None): demoMicroFlowGraphApp.current.pack_forget()
            demoMicroFlowGraphApp.current=demoMicroFlowGraphApp.frames["Settings"]
            demoMicroFlowGraphApp.frames["Settings"].pack(in_=self.rightContainter, side=tk.TOP, fill=BOTH, expand=True)


        self.bind("<Control-h>", jumpHelp); self.bind("<Control-H>", jumpHelp)
        self.bind("<Control-g>", jumpGraph); self.bind("<Control-G>", jumpGraph)
        self.bind("<Control-q>", forceQuit); self.bind("<Control-Q>", forceQuit)
        self.bind("<Control-r>", jumpSettings); self.bind("<Control-R>", jumpSettings) 


#Driver Code: 
app = demoMicroFlowGraphApp()
app.mainloop()
